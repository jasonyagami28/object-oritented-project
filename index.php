<?php

require_once 'animal.php';
require_once 'frog.php';
require_once 'ape.php';




$sheep = new Animal("Shaun", 2, "False");


echo "Nama Domba: ". $sheep->name; // "shaun"
echo "<br>";
echo "Jumlah Kaki: ". $sheep->legs; // 2
echo "<br>";
echo "Berdarah Dingin? ". $sheep->cold_blooded; // false
echo "<br><br>";



$sungokong = new Ape("Kera Sakti", 2, "False");
echo "Nama Monyet: ". $sungokong->name; // "kera sakti"
echo "<br>";
echo "Jumlah Kaki: ". $sungokong->legs; // 2
echo "<br>";
echo "Berdarah Dingin? ". $sungokong->cold_blooded; // false
echo "<br>";
$sungokong->yell(); // "Auooo"
echo "<br><br>";

$kodok = new Frog("Buduk", 2, "True");
echo "Nama Kodok: ". $kodok->name; // "shaun"
echo "<br>";
echo "Jumlah Kaki: ". $kodok->legs; // 2
echo "<br>";
echo "Berdarah Dingin? ". $kodok->cold_blooded ;// false
echo "<br>";
$kodok->jump() ; // "hop hop"
echo "<br><br>";
echo "<br><br>";

var_dump($kodok);
echo "<br><br>";
var_dump($sungokong);
echo "<br><br>";
var_dump($sheep);


// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

?>